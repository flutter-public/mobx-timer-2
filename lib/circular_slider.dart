import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import 'timer/count_down.dart';
import 'utils.dart';

final customWidth =
    CustomSliderWidths(trackWidth: 4, progressBarWidth: 20, shadowWidth: 40);

final customColors = CustomSliderColors(
    dotColor: HexColor('#FFB1B2'),
    trackColor: HexColor('#E9585A'),
    progressBarColors: [HexColor('#FB9967'), HexColor('#E9585A')],
    shadowColor: HexColor('#FFB1B2'),
    shadowMaxOpacity: 0.05);

final CircularSliderAppearance sliderAppearance = CircularSliderAppearance(
    customWidths: customWidth,
    customColors: customColors,
    startAngle: 270,
    angleRange: 360,
    spinnerMode: false,
    size: 360.0);

//----------------------- Second slider
final customWidthSecondSlider =
    CustomSliderWidths(trackWidth: 1, progressBarWidth: 2);
final customColorsSecondSlider = CustomSliderColors(
    trackColor: Colors.white,
    progressBarColor: Colors.orange,
    hideShadow: true);

final CircularSliderAppearance appearanceSecondSlider =
    CircularSliderAppearance(
        customWidths: customWidthSecondSlider,
        customColors: customColorsSecondSlider,
        startAngle: 270,
        angleRange: 380,
        size: 240.0,
        animationEnabled: false);

//-----------------------

class CountDownSlider extends StatefulWidget {
  @override
  _CountDownSliderState createState() => _CountDownSliderState();
}

class _CountDownSliderState extends State<CountDownSlider> {
  final CountDown countDown = CountDown();
  @override
  Widget build(BuildContext context) {
    return Observer(
        builder: (_) => Container(
            color: Colors.black,
            child: Column(
              children: <Widget>[
                Container(
                  height: 150,
                  child: countDown.state == StateTimer.alarm
                      ? Icon(
                    Icons.notifications_active,
                    color: Colors.yellow,
                    size: 100,
                  )
                      : Container(),
                ),
                getSlider(countDown),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[getButton(countDown)],
                )
              ],
            )));
  }
}

Widget getSlider(CountDown countDown) {
  if (countDown.state == StateTimer.running ||
      countDown.state == StateTimer.alarm) {
    return SafeArea(
      child: Center(
          child: SleekCircularSlider(
              appearance: sliderAppearance,
              min: 0,
              max: 3600,
              initialValue: countDown.ellapsed.toDouble(),
              innerWidget: (double value) {
                return getTimer(countDown);
              })),
    );
  } else {
    return SafeArea(
      child: Center(
          child: SleekCircularSlider(
              onChangeEnd: (double value) {
                countDown.setDuration(value.floor());
              },
              appearance: sliderAppearance,
              min: 0,
              max: 3600,
              initialValue: countDown.ellapsed.toDouble(),
              innerWidget: (double value) {
                return getTimer(countDown);
              })),
    );
  }
}

Widget getTimer(CountDown countDown) {
  return Align(
      alignment: Alignment.center,
      child: SleekCircularSlider(
        appearance: appearanceSecondSlider,
        min: 0,
        max: 59,
        initialValue: countDown.seconds.toDouble(),
        innerWidget: (double value) {
          return Center(
            child: Text(
                "${printDuration(Duration(seconds: countDown.ellapsed))}",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 50.0,
                    fontWeight: FontWeight.w600)),
          );
        },
      ));
}

Widget getButton(CountDown countDown) {
  if (countDown.state == StateTimer.ready)
    return buildButton(Icons.play_arrow, Event.start, countDown);

  if (countDown.state == StateTimer.running) {
    return Row(
      children: <Widget>[
        buildButton(Icons.pause, Event.pause, countDown),
        buildButton(Icons.replay, Event.reset, countDown)
      ],
    );
  }
  if (countDown.state == StateTimer.paused) {
    return Row(
      children: <Widget>[
        buildButton(Icons.play_arrow, Event.start, countDown),
        buildButton(Icons.replay, Event.reset, countDown)
      ],
    );
  }
  if (countDown.state == StateTimer.alarm)
    return buildButton(Icons.replay, Event.reset, countDown);
  return Container();
}



Widget buildButton(IconData iconData, event, CountDown countDown) {
  return Padding(
    padding: const EdgeInsets.all(30),
    child: CircleAvatar(
      radius: 30,
      backgroundColor: Colors.blueAccent,
      child: IconButton(
        icon: Icon(
          iconData,
          color: Colors.white,
        ),
        onPressed: () => {
          if (event == Event.start) countDown.onStart()
          else if(event == Event.reset) countDown.onReset()
          else if(event == Event.pause) countDown.onPause()
        },
      ),
    ),
  );
}
