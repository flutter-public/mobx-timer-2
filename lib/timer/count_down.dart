import 'package:mobx/mobx.dart';
import 'dart:async';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:flutter_beep/flutter_beep.dart';
part 'count_down.g.dart';

class CountDown = _CountDown with _$CountDown;
enum StateTimer { reset, ready, running, paused, alarm }
enum Event { dial, start, reset, pause, reach_zero }

abstract class _CountDown with Store {
  Timer _timer;
  static const one_second = const Duration(seconds: 1);

  @observable
  StateTimer state = StateTimer.reset;

  @observable
  int ellapsed = 0;

  @observable
  int seconds = 0;

  @action //Sự kiện người dùng dial slider để đặt thời gian duration
  setDuration(int newDuration) {
    ellapsed = newDuration;
    seconds = ellapsed%60;
    if(state == StateTimer.reset){
      state = StateTimer.ready;
    }
  }

  @action
  onStart() {
    //Sự kiện người dùng tap nút Start
    handleEvent(Event.start);
  }

  @action
  onReset() {
    //Sự kiện người dùng tap nút Reset
    handleEvent(Event.reset);
  }

  @action
  onPause() {
    //Sự kiện người dùng tap nút Pause
    handleEvent(Event.pause);
  }

  void tickCallBack(timer) {
    if (ellapsed > 0) {
      ellapsed--;
      seconds = ellapsed % 60;
      FlutterBeep.beep();
    } else {
      timer.cancel();
      handleEvent(Event.reach_zero);
    }
  }

  void  handleEvent(Event event) {
    switch (state) {
      case StateTimer.reset:
        if (event == Event.dial) {
          state = StateTimer.ready;
        }
        break;
      case StateTimer.ready:
        if (event == Event.start) {
          state = StateTimer.running;
          _timer = Timer.periodic(one_second, tickCallBack);
        } else if (event == Event.dial) {
          //Still in ready state
        }
        break;
      case StateTimer.running:
        if (event == Event.pause) {
          _timer.cancel();
          state = StateTimer.paused;
        } else if (event == Event.reset) {
          _timer.cancel();
          state = StateTimer.reset;
          ellapsed = 0;
          seconds = 0;
        } else if (event == Event.reach_zero) {
          state = StateTimer.alarm;
          FlutterRingtonePlayer.playNotification();
        }
        break;
      case StateTimer.paused:
        if (event == Event.start) {
          state = StateTimer.running;
          _timer = Timer.periodic(one_second, tickCallBack);
        } else if (event == Event.reset) {
          _timer.cancel();
          state = StateTimer.reset;
          ellapsed = 0;
          seconds = 0;
        }
        break;
      case StateTimer.alarm:
        if (event == Event.reset) {
          FlutterRingtonePlayer.stop();
          state = StateTimer.reset;
          ellapsed = 0;
          seconds = 0;
        }
        break;
    }
  }
}
